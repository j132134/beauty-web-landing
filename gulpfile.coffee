require 'coffee-script'

path = require 'path'
del = require 'del'
gulp = require 'gulp'
filter = require 'gulp-filter'
jade = require 'gulp-jade'
stylus = require 'gulp-stylus'
base64 = require 'gulp-css-base64'
webpack = require 'webpack'
stream = require 'webpack-stream'
sourcemaps = require 'gulp-sourcemaps'
rename = require 'gulp-rename'
replace = require 'gulp-replace'
connect = require 'gulp-connect'
rewrite = require 'connect-modrewrite'

## variables
config =
  gulp: require './gulp.config'
  webpack: require './webpack.config'

option = config.gulp.option
src = config.gulp.path.src
dist = config.gulp.path.dist

hash = undefined
env = undefined

error = ->
  @emit 'end'

## private tasks
gulp.task 'clean', ->
  del dist.root

gulp.task 'copy', ->
  gulp.src path.join src.iamge, '**/*'
  .pipe gulp.dest dist.img

  gulp.src path.join __dirname, 'legacy/**/*'
  .pipe gulp.dest dist.root

# build
gulp.task 'jade:dev', ->
  gulp.src path.join src.jade, '**/*.jade'
  .pipe filter ['**/*.jade', '!user/**/*.jade']
  .pipe jade option.jade.dev
  .on 'error', error
  .pipe gulp.dest dist.root
  .pipe connect.reload()

gulp.task 'jade', ->
  gulp.src path.join src.jade, '**/*.jade'
  .pipe filter ['**/*.jade', '!user/**/*.jade']
  .pipe jade option.jade.prod
  .pipe gulp.dest dist.root

gulp.task 'stylus:dev', ->
  gulp.src path.join src.stylus, '*.styl'
  .pipe sourcemaps.init()
  .pipe stylus option.stylus.dev
  .on 'error', error
  .pipe base64 option.base64
  .pipe sourcemaps.write '.'
  .pipe gulp.dest dist.css
  .pipe connect.reload()

gulp.task 'stylus', ->
  gulp.src path.join src.stylus, '*.styl'
  .pipe stylus option.stylus.prod
  .pipe base64 option.base64
  .pipe gulp.dest dist.css

gulp.task 'webpack:dev', ->
  gulp.src path.join src.coffee, 'app/entry.coffee'
  .pipe stream config.webpack.dev, webpack
  .on 'error', error
  .pipe gulp.dest dist.js
  .pipe connect.reload()

gulp.task 'webpack', ->
  gulp.src path.join src.coffee, 'app/entry.coffee'
  .pipe stream config.webpack.prod, webpack
  .pipe gulp.dest dist.js

# serve
gulp.task 'watch', ['copy', 'jade:dev', 'stylus:dev', 'webpack:dev'], ->
  gulp.watch 'assets/**/*', ['copy']
  gulp.watch '**/*.jade', {cwd: src.jade}, ['jade:dev']
  gulp.watch '**/*.styl', {cwd: src.stylus}, ['stylus:dev']
  gulp.watch '**/*', {cwd: src.coffee}, ['webpack:dev']

gulp.task 'connect', ->
  connect.server
    root: dist.root
    port: 8000
    livereload: true
    fallback: path.join dist.root, 'app.html'
    middleware: ->
      return [
        rewrite [
          '^/?$      /user.html  [L]'
#         '^/shop/$  /shop.html  [L]'
          '^/content/(.*)$  https://mapi.heybeauty.kr/web/contents/$1  [P]'
        ]
      ]

# build
gulp.task 'replace:html', ['jade'], ->
  gulp.src path.join dist.root, '*.html'
  .pipe replace /\/(app|user|shop)\.(css|js)/g, "/$1.#{hash}.$2"
  .pipe gulp.dest dist.root

gulp.task 'replace:js', ['webpack'], ->
  gulp.src path.join dist.js, '*.js'
  .pipe replace 'replace_facebook_app_id', if env is 'production' then '1756233921270925' else '1806149346279382'
  .pipe replace 'T8YgEgUdPbDJikeG8HHJ', 'FbTDsDQb5ZXRY8vQ4yPB'
  .pipe replace 'http://lvh.me:8000/', 'http://heybeauty.me/'
  .pipe gulp.dest dist.js

gulp.task 'rename', ['stylus', 'webpack', 'replace:js'], ->
  gulp.src [
    path.join dist.css, '*.css'
    path.join dist.js, '*.js'
  ]
  .pipe rename (path) ->
    path.basename += ".#{hash}"
  .pipe gulp.dest (file) ->
    return file.base

gulp.task 'robots', ->
  gulp.src path.join src.robots, "#{env}.txt"
  .pipe rename 'robots.txt'
  .pipe gulp.dest dist.root

  gulp.src path.join __dirname, 'assets/*.html'
  .pipe gulp.dest dist.root


## public tasks
gulp.task 'default', ->
  console.log 'Run "gulp serve" or "gulp build"'

gulp.task 'serve', ['clean'], ->
  gulp.start ['copy', 'jade:dev', 'stylus:dev', 'webpack:dev', 'watch', 'connect']

gulp.task 'build', ['clean'], ->
  hash = (new Date).getTime()
  index = process.argv.indexOf '--env'
  param = process.argv[++index]
  env = index and param or 'production'
  gulp.start ['copy', 'jade', 'stylus', 'webpack', 'replace:html', 'replace:js', 'rename', 'robots']