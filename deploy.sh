#!/bin/bash

case $1 in
"production")
	ssh prod0 "cd ~/beauty-web-landing/; git checkout master; git pull; git fetch -t; git checkout $(git for-each-ref refs/tags --sort=taggerdate --format="%(refname:short)"|tail -1); npm update; bower update; gulp build"
;;
"staging")
	echo -n "npm update? (y/N)"
	read npm_update
	echo -n "bower update? (y/N)"
	read bower_update

	[[ $npm_update == [yY] ]] && npm="npm update;"
	[[ $bower_update == [yY] ]] && bower="bower update;"

	ssh staging "cd ~/beauty-web-landing/; git pull origin develop;" $npm $bower "gulp build --env staging"
;;
"sync")
	gulp build --env staging; rsync -avz dist/ staging:~/beauty-web-landing/dist
;;
esac
