window.events = {}
window.initDelay = 1150

window.noDuration = (target, callback) ->
  target.css 'transition', '0s'
  callback()
  _.delay ->
    target.css 'transition', ''
    
window.animateText = (target, delay = 0, weight = 1) ->
  elem = $('#' + target)

  h2 = elem.find('h2').not(':hidden')
  p = elem.find('p').not(':hidden').eq(0)
  download = elem.find('.download')

  h2.hide().css {top: '85px', opacity: '0'}
  p.hide().css {top: '55px', opacity: '0'}
  download.hide()

  _.delay ->
    h2.show().animate {top: '0', opacity: '1'}, {duration: 450 * weight}
    _.delay ->
      p.show().animate {top: '0', opacity: '1'}, {duration: 550 * weight}
      download.fadeIn 700
    , 100 * weight
  , delay

window.close =
  callback: undefined
  register: (cb) ->
    @callback = cb
  call: ->
    @callback()
    @callback = undefined

## load page animate functions
requireDir = (context) -> context.keys().forEach context
requireDir require.context './pages/', false, /\.coffee$/

## variables
sections = []
total = 0
current = 0
indicator = undefined
lastAnimation = 0
duration = 450
quietTime = duration + 800

touchStartX = undefined
touchMoveX = undefined
moveX = undefined

skipAnimation = 0

## init
$(document).ready init = ->

  window.isMobile = isMobile = if (`'ontouchstart' in window` or navigator.maxTouchPoints) then true else false
  do ->
    $('body').addClass if isMobile then 'horizontal' else 'vertical'
    return if not isMobile

    setOrientation = undefined
    do setOrientation = ->
      stringOrientation = if window.orientation is 0 then 'portrait' else 'landscape'
      if stringOrientation is 'landscape'
        $('meta[name="viewport"]').attr 'content', 'width=1680, user-scalable=no'

    $(window).on 'orientationchange', setOrientation

  sections = $('section')
  total = sections.length
  indicator = $('nav ul')

  $('h1, nav, a.shop').css 'z-index', total + 1
  $('#init').css 'z-index', total + 2

  sections.each (i, s) ->
    $(s).css 'z-index', total - i
    indicator.append "<li>#{i+1}</li>"

  indicator.find('li').on 'click', (e) ->
    page.to indicator.find('li').index $(e.target)

  setTimeout ->
    $('#init').fadeOut initDelay
    setIndicator()
    window.initDelay = 100
  , 450


## paging
page =
  up: ->
    return if current is 0
    sections.eq(--current).removeClass 'scrolled'
    setIndicator()


  down: ->
    return if current is total - 1
    sections.eq(current++).addClass 'scrolled'
    setIndicator()

  to: (to) ->
    times = Math.abs(current - to)
    skipAnimation = times
    if to < current
      for t in [1..times]
        window.setTimeout ->
          page.up()
        , t * duration / 40
    else if to > current
      for t in [1..times]
        window.setTimeout ->
          page.down()
        , t * duration / 40

setIndicator = ->
  return if skipAnimation and --skipAnimation
  indicator.find('li').removeClass('active').eq(current).addClass('active')
  loadFunction current

loadFunction = (index) ->
  ga('send', 'event', 'User', 'paging', "section#{index+1}")

  name = sections.eq(index).get(0).id
  theme = sections.eq(index).attr('data-theme')
  setTheme theme
  close.call() if close.callback
  events[name]() if events[name]

setTheme = (type) ->
  reverse = if type is 'black' then 'white' else 'black'
  $('h1, nav, a.shop').removeClass(reverse).addClass(type)


## bind scroll event
$(document).on 'mousewheel DOMMouseScroll MozMousePixelScroll', (event) ->
  originalEvent = event.originalEvent
  return if Math.abs(originalEvent.wheelDeltaX or 0) > Math.abs(originalEvent.wheelDeltaY or 0)
  delta = originalEvent.wheelDelta or -originalEvent.detail
  event.preventDefault()
  init_scroll event, delta

init_scroll = (event, delta) ->
  timeNow = (new Date).getTime()
  if timeNow - lastAnimation < quietTime
    event.preventDefault()
  else
    lastAnimation = timeNow
    if delta < 0 then page.down() else page.up()


## bind touch event
$(document).on 'touchstart', (e) ->
  e.stopPropagation()
  touchStartX = Math.floor e.originalEvent.targetTouches[0].pageX

$(document).on 'touchmove', (e) ->
  e.stopPropagation()
  e.preventDefault()
  touchMoveX = Math.floor e.originalEvent.targetTouches[0].pageX
  moveX = touchMoveX - touchStartX

$(document).on 'touchend', (e) ->
  e.stopPropagation()
  if Math.abs(moveX) < 100
    console.log '옆으로 넘겨주세요'
    return
  if moveX < 0 then page.down() else page.up()


## bind keyboard event
$(document).on 'keydown', (e) ->
  tag = e.target.tagName.toLowerCase()
  switch e.which
    when 40 then page.down() if (tag isnt 'input' and tag isnt 'textarea') # down
    when 34 then page.down() if (tag isnt 'input' and tag isnt 'textarea') # page down
    when 32 then page.down() if (tag isnt 'input' and tag isnt 'textarea') # space bar
    when 38 then page.up() if (tag isnt 'input' and tag isnt 'textarea') # up
    when 33 then page.up() if (tag isnt 'input' and tag isnt 'textarea') # page up
    when 36 then page.to(0) # home
    when 35 then page.to(total - 1) # end