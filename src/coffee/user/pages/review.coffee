$.ajax
  type: 'GET'
  url: 'http://assets.heybeauty.me/kr/web/reviews.json?callback=?'
  async: false
  jsonpCallback: 'jsonCallback'
  contentType: "application/json"
  dataType: 'jsonp'
  success: (data) ->
    _.each data.review.items, (item) ->
      div = $('<div class="box"><p></p><p></p></div>')
      body = div.find('p').eq(0)
      user = div.find('p').eq(1)

      body.addClass('body').text item.body
      user.addClass('user').text item.user

      div.hide().appendTo '.reviews'

events['review'] = ->

  animateText 'review', 200 if not isMobile

  rotateReview = undefined
  landscape = undefined
  portrait = undefined

  do rotateReview = ->
    clearInterval landscape if landscape
    clearInterval portrait if portrait

    width = $(document).innerWidth()
    height = $(document).innerHeight()

    if width > height or width >= 800
      reviews = $('#review').find('.box').hide()
      reviews.eq(0).fadeIn 425
      reviews.eq(1).fadeIn 425

      i = 1
      total = Math.ceil(reviews.length / 2)
      landscape = setInterval ->
        reviews.fadeOut 1100
        reviews.eq(2 * i).fadeIn 1150
        reviews.eq(2 * i + 1).fadeIn 1150

        i = 0 if ++i is total
      , 3300

      ## destory
      close.register ->
        clearInterval landscape

    else
      reviews = $('#review').find('.box').hide()
      reviews.eq(0).fadeIn 350

      i = 1
      total = reviews.length
      portrait = setInterval ->
        reviews.fadeOut 900
        reviews.eq(i).fadeIn 950

        i = 0 if ++i is total
      , 2900

      ## destory
      close.register ->
        clearInterval portrait

  $(window).on 'orientationchange', rotateReview