events['download'] = ->
  return if isMobile

  delay = []

  ## text
  animateText 'download', 450, 1.25

  links = $('.links')
  links.hide()

  delay.push setTimeout ->
    links.fadeIn 750
  , 950

  ## destory
  close.register ->
    _.each delay, (d) ->
      clearTimeout d