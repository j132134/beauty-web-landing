Vivus = require 'vivus'

menu = new Vivus 'menu',
  duration: 35
  delay: 20
  start: 'manual'

events['service'] = ->
  return if isMobile

  delay = []

  ## text
  animateText 'service', 200

  ## animate svg
  menu.stop().reset()
  delay.push setTimeout ->
    menu.play()
  , 200

  ## drop frame
  frame = $('#service .frame')
  noDuration frame, ->
    frame.addClass 'ready'
  images = frame.find('img')
  images.hide()

  delay.push setTimeout ->
    frame.removeClass 'ready'
    delay.push setTimeout ->
      images.fadeIn 250
    , 300
  , 1300

  ## destory
  close.register ->
    menu.stop().reset()
    _.each delay, (d) ->
      clearTimeout d