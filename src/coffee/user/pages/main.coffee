events['main'] = ->
  return if isMobile

  delay = []

  main = $('#main')
  elements = main.find('.img, h2, .download')

  img = main.find('.img')
  h2 = main.find('h2')
  download = main.find('.download')

  elements.hide()

  img.css {bottom: '-50%', opacity: '0'}
  h2.css {top: '52%', opacity: '0'}

  delay.push setTimeout ->
    img.show().animate {bottom: '0', opacity: '1'}, {duration: 450}
    h2.show().animate {top: '50%', opacity: '1'}, {duration: 530}

    delay.push setTimeout ->
      download.show()
    , 450
  , initDelay * 0.7

  ## destory
  close.register ->
    _.each delay, (d) ->
      clearTimeout d