Vivus = require 'vivus'

line = new Vivus 'lineOnTime',
  type: 'scenario'
  file: '/img/user/demo/line_grey.svg'
  start: 'manual'

events['time'] = ->
  return if isMobile

  delay = []

  ## text
  animateText 'time'

  ## location
  flicker = $('#time .location .flicker')
  flick = ->
    noDuration flicker, ->
      flicker.show()
      .addClass 'shrink'

    delay.push setTimeout ->
      flicker.removeClass 'shrink'
      delay.push setTimeout ->
        flicker.fadeOut 450
      , 550
    , 100

  delay.push setTimeout flick, 0
  delay.push setTimeout flick, 1600

  ## pin
  pins = $('#time .pins span')
  noDuration pins, ->
    pins.show()
    .addClass 'shrink'

  _.each pins, (pin, i) ->
    delay.push setTimeout ->
      $(pin).removeClass 'shrink'
    , i * 270 + 50

  ## draw phone
  line.stop().reset()
  delay.push setTimeout ->
    line.play()
  , 700

  ## drop frame
  frame = $('#time .frame')
  noDuration frame, ->
    frame.addClass 'ready'
  images = frame.find('img')
  images.hide()

  delay.push setTimeout ->
    frame.removeClass 'ready'
    pins.fadeOut 450
    delay.push setTimeout ->
      images.fadeIn 250
    , 300
  , 3000

  ## destory
  close.register ->
    line.stop().reset()
    _.each delay, (d) ->
      clearTimeout d