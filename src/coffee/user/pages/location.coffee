Vivus = require 'vivus'

path = new Vivus 'path',
  duration: 80
  animTimingFunction: Vivus.EASE_OUT_BOUNCE
  start: 'manual'

line = new Vivus 'lineOnLocation',
  type: 'scenario'
  file: '/img/user/demo/line_grey.svg'
  start: 'manual'

events['location'] = ->
  return if isMobile

  delay = []

  ## text
  animateText 'location', 250

  ## path
  path.stop().reset()
  delay.push setTimeout ->
    path.play()
  , 900

  ## flicker
  flicker = $('#location .me .flicker')
  flick = ->
    noDuration flicker, ->
      flicker.show()
      .addClass 'shrink'

    delay.push setTimeout ->
      flicker.removeClass 'shrink'
      delay.push setTimeout ->
        flicker.fadeOut 450
      , 550
    , 100

  delay.push setTimeout flick, 0
  delay.push setTimeout flick, 1800

  ## draw phone
  line.stop().reset()
  delay.push setTimeout ->
    line.play()
  , 1650

  ## drop frame
  frame = $('#location .frame')
  noDuration frame, ->
    frame.addClass 'ready'
  images = frame.find('img')
  images.hide()

  delay.push setTimeout ->
    frame.removeClass 'ready'
    delay.push setTimeout ->
      images.fadeIn 250
    , 300
  , 3500

  ## destory
  close.register ->
    path.stop().reset()
    line.stop().reset()
    _.each delay, (d) ->
      clearTimeout d