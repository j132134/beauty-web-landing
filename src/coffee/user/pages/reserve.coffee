events['reserve'] = ->
  return if isMobile

  delay = []

  ## text
  animateText 'reserve', 200

  ## fit blur image size
  resizeBlurImg = undefined
  do resizeBlurImg = ->
    $('#reserve').find('.blur').find('.source').css 'background-size', ->
      width = $(document).innerWidth()
      height = $(document).innerHeight()

      if width >= (height * 16 / 9)
        return "#{width}px auto"
      else
        return "auto #{height}px"
  $(window).on 'resize', resizeBlurImg

  ## drop frame
  frame = $('#reserve .frame')
  noDuration frame, ->
    frame.addClass 'ready'
  images = frame.find('img')
  images.hide()
  images.eq(2).css 'transform', 'scale(0)'

  delay.push setTimeout ->
    frame.removeClass 'ready'
    delay.push setTimeout ->
      images.eq(0).fadeIn 100

      delay.push setTimeout ->
        images.eq(2).show()
        delay.push setTimeout ->
          images.eq(2).css 'transform', 'scale(1)'
        , 0
        delay.push setTimeout ->
          images.eq(1).show()
        , 300 # after confirm popup transition duration
        delay.push setTimeout ->
          images.eq(2).css 'transition-duration', '500ms'
          images.eq(2).show().css 'transform', 'translateY(100%)'
          delay.push setTimeout ->
            images.eq(2).css 'transition-duration', ''
          , 500
        , 900 # slide down
      , 550 # second popup
    , 300 # init page fade in delay
  , 1200 # start animation delay

  ## destory
  close.register ->
    _.each delay, (d) ->
      clearTimeout d