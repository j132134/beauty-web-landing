module.exports = {
  toArray (data) {
    const description = '이제 헤이뷰티 앱으로 쉽고 빠르게 예약하세요';

    let result = [
      {name: 'title', content: data.name},
      {name: 'description', content: description},
      {name: 'og:title', content: data.name},
      {name: 'og:description', content: description},
      {name: 'og:site_name', content: data.en_name || data.name},
      {name: 'og:url', content: `http://heybeauty.me/shop/${data.en_name || data.id}`},
      {name: 'og.type', content: 'website'},
      {name: 'fb:app_id', content: 'replace_facebook_app_id'}
    ];

    if (data.image_url)
      result.push({name: 'og:image', content: data.image_url});

    return result
  }
};