import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

import shop from './modules/shop'

export default new Vuex.Store({
  state: {
    service_categories: [],
    categories: [],
    events: [],
    banners: []
  },
  getters: {
    service_categories: state => {
      return state.service_categories;
    },
    service_category_name: (state, getters) => id => {
      const category = getters.service_categories.find(category => category.id === id);
      return category ? category.name : null;
    },
    categories: state => {
      return state.categories;
    },
    events: state => {
      return state.events;
    },
    banners: state => {
      return state.banners;
    }
  },
  mutations: {
    assign (state, data) {
      _.assign(state, data);
    }
  },
  actions: {
    setMeta ({ commit }, data) {
      commit('assign', data);
    }
  },
  modules: {
    shop
  }
})