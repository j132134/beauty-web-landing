export default {
  state: {
    services: [],
    portfolios: [],
    posts: [],
    nearby_shops: [],
    shop_info: {}
  },
  getters: {
    services (state) {
      return state.services;
    },
    portfolios (state) {
      return state.portfolios;
    },
    posts (state) {
      return state.posts;
    },
    nearby (state) {
      return state.nearby_shops;
    },
    shop_info (state) {
      return state.shop_info;
    }
  },
  mutations: {
    setData (state, data) {
      data.shop_info.category_name = (_.find(data.categories, {id: data.shop_info.category_id_list[0]}) || {}).name;
      if (!data.shop_info.image_url) data.shop_info.image_url = `http://assets.heybeauty.me/kr/web/cover/original/cat_${data.shop_info.category_id_list[0]}_${_.random(0, 3)}.jpg`;
      _.assign(state, data);
    }
  },
  actions: {
    getShop ({ commit, rootState }, id) {
      return new Promise((resolve, reject) => {
        $.get(`https://api.heybeauty.kr/web/shops/${id}`).then(
          (response) => {
            commit('setData', _.assign(response.data, {categories: rootState.categories}));
            resolve()
          },
          (response) => {
            console.error('샵 정보를 가져오는데 에러가 발생했습니다.');
            console.debug(response);
            reject()
          }
        )
      })
    }
  }
}