import Vue from 'vue'
import VueHead from 'vue-head'
import VueRouter from 'vue-router'

Vue.use(VueHead);
Vue.use(VueRouter);

import { date } from './filters/date'
import { phone } from './filters/phone'
import { currency } from './filters/currency'
import { distance } from './filters/distance'

Vue.filter('date', date);
Vue.filter('phone', phone);
Vue.filter('currency', currency);
Vue.filter('distance', distance);

window.HB = {};
HB.is_mobile = window.innerWidth < 700;

import store from './vuex/store'
import routes from './routes'

const router = new VueRouter({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    else if (to.hash) {
      return { selector: to.hash }
    }
    else if (!_.includes(to.name, 'popup') && !_.includes(from.name, 'popup')) {
      return { y: 0 }
    }
  },
  routes
});

router.beforeEach((to, from, next) => {
  to.path.length > 1 && (/\/$/).test(to.path) ? next({path: to.path.slice(0, -1)}) : next()
});

const app = new Vue({
  store,
  router
}).$mount('#app');