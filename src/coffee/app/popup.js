module.exports = {
  open (url) {
    const width = '375';
    const height = '720';
    const top = (screen.height / 2) - (height / 2);
    const left = (screen.width / 2) - (width / 2);
    window.open(url, '', `width=${width}, height=${height}, top=${top}, left=${left}`);
  }
};