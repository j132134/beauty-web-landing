export default [
  {
    path: '/shop/:id',
    component: require('./components/shop.common'),
    children: [
      {
        path: '',
        name: 'shop.info',
        component: require('./components/shop.info')
      },
      {
        path: 'service',
        alias: 'portfolio',
        name: 'shop.service',
        component: require('./components/shop.service'),
        children: [
          {
            path: '/shop/:id/portfolio/:portfolio_id',
            name: 'popup.portfolio',
            component: require('./components/popup.portfolio'),
          },
          {
            path: ':service_id',
            name: 'popup.service',
            component: require('./components/popup.service'),
          }
        ]
      },
      {
        path: 'reviews',
        name: 'shop.review',
        component: require('./components/shop.review')
      }
    ]
  },
  {
    path: '*',
    component: {
      template: '<div class="error e404"></div>'
    }
  }
]