export function distance (d) {
  if (d < 1)
    return _.round(d, 2) * 1000 + 'm';
  else
    return _.round(d, 1) + 'km';
}