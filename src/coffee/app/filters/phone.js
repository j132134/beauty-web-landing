export function phone (value) {
  if (value)
    return value.toString().replace(/(050\d)(\d{3,4})(\d{4})/g, '$1-$2-$3')
}