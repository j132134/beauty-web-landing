import moment from 'moment'

export function date (date) {
  return moment(date, 'X').format('YYYY.MM.DD')
}