let interval;

function setIndex(el, index) {
  const image = $(el).find('.images').children();
  const nav = $(el).find('.nav').children();

  image.css('transform', `translateX(${-100 * index}%)`);
  nav.removeClass('active').eq(index).addClass('active');
}

module.exports = {
  bind (el, binding) {
    let i;
    const length = binding.value.data.length;

    setIndex(el, i = 0);

    interval = setInterval(() => {
      setIndex(el, i++);
      if (i == length) i = 0
    }, 3500);
  },
  unbind () {
    clearInterval(interval);
  }
};