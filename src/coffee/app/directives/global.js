module.exports = {
  bind () {
    const $window = $(window);
    $window.on('resize', _.throttle(() => {
      if ((HB.is_mobile && window.innerWidth >= 700) || (!HB.is_mobile && window.innerWidth < 700))
        location.reload();
    }, 200));
  },
  unbind () {
    $(window).off('resize');
  }
};