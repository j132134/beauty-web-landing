function setIndex(el, index, move, option) {
  const children = $(el).children();
  const visible = children.filter(':visible');
  const length = children.length;
  let duration = 0;

  option = _.defaults(option, {
    animate: false,
    edge: false
  });

  if (option.animate) {
    duration = Math.floor(250 * (window.innerWidth - Math.abs(move)) / window.innerWidth);
    visible.css('transition-duration', `${duration}ms`);
    visible.css('transform', move > 0 ? 'translateX(100%)' : 'translateX(-100%)')
  }
  else {
    duration = Math.floor(option.edge ? 300 : 1000 * Math.abs(move) / window.innerWidth);
    visible.css('transition-duration', `${duration}ms`);
    visible.css('transform', 'translateX(0)');
  }

  setTimeout(() => {
    children.css('transition-duration', '');
    children.css({left: '', transform: ''}).hide();
    children.eq(index).show();
    if (index < length - 1) children.eq(index + 1).css('left', '103.5%').show();
    if (index > 0) children.eq(index - 1).css('left', '-103.5%').show();
  }, duration);

  return duration;
}

module.exports = {
  bind (el, binding) {
    const current = binding.value.current;
    const criterion = Math.floor(window.innerWidth * 0.1);

    let captured = false;
    let prevent = false;
    let start = null;
    let move = 0;
    let last;

    if (HB.is_mobile) {
      $(el).on('touchstart', (event) => {
        if (prevent) return;
        move = 0;
        captured = true;
        start = event.originalEvent.touches[0].pageX;
        if (!last) last = $(el).children().length - 1;
      });

      $(el).on('touchmove', _.throttle((event) => {
        if (event.originalEvent.touches.length > 1) return;
        if (prevent || !captured) return;

        move = event.originalEvent.touches[0].pageX - start;

        if ((current.page == 0 && move > 0) || (current.page == last && move < 0))
          move = Math.floor(move / 1.8);

        $(el).children(':visible').css('transform', `translateX(${move}px)`);
      }, 10));

      $(el).on('touchend', (event) => {
        if (!captured) return;
        if (Math.abs(move) > 5) event.preventDefault();

        let duration;
        if ((current.page == 0 && move > 0) || (current.page == last && move < 0))
          duration = setIndex(el, current.page, move, {edge: true});
        else if (Math.abs(move) > criterion)
          duration = setIndex(el, move > 0 ? current.page -= 1 : current.page += 1, move, {animate: true});
        else
          duration = setIndex(el, current.page, move);

        prevent = true;
        setTimeout(() => {
          prevent = false;
          captured = false;
        }, duration);
      });
    }
  },
  update (el, binding) {
    if (!_.isNull(binding.oldValue.toIndex) && binding.value.toIndex !== binding.oldValue.toIndex) {
      const move = binding.value.toIndex > binding.oldValue.toIndex ? -1 : 1;
      setIndex(el, binding.value.current.page = binding.value.toIndex, move, {animate: true});
    }
    if (binding.value.key !== binding.oldValue.key) {
      setIndex(el, binding.value.current.page = 0);
    }
  },
  componentUpdated (el, binding) {
    if (binding.value.toIndex === 0 && _.isNull(binding.oldValue.toIndex)) {
      setIndex(el, binding.value.current.page = 0, 0);
    }
  },
  unbind (el) {
    $(el).off('touchstart touchmove touchend');
  }
};