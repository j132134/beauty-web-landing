module.exports = {
  bind (el) {
    const cover = $(el).find('.cover');
    const hgroup = $(el).find('hgroup>*');
    const height = HB.is_mobile ? window.innerWidth / 2 : 267 + 57;

    $(document).on('scroll', _.throttle(() => {
      const scroll = $(document).scrollTop() / height;
      const ratio = $(document).scrollTop() / (height - 44);

      if (scroll < 1)
        $(el).removeClass('scrolled');
      else
        $(el).addClass('scrolled');

      cover.toggleClass('hidden', ratio < .1);

      if (HB.is_mobile) {
        hgroup.css('opacity', Math.max(0, 1 - ratio));

        cover.css({
          opacity: Math.min(ratio, 1),
          'line-height': `${Math.max(44, height * (1 - scroll))}px`
        });
      }
    }, 10));
  },
  unbind () {
    $(document).off('scroll');
  }
};