module.exports = {
  bind () {
    $('body').css('overflow', 'hidden');
  },
  unbind () {
    $('body').css('overflow', '');
  }
};