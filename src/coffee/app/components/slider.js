module.exports = {
  props: ['type', 'list', 'desc'],
  template: require('../template/slider.pug'),
  data () {
    let count;

    if (this.type == 'shop')
      count = HB.is_mobile ? 2 : 4;
    else if (this.desc == 'popup')
      count = HB.is_mobile ? 4 : 6;
    else
      count = HB.is_mobile ? 4 : 8;

    return {
      count: count,
      current: {page: 0},
      toIndex: null
    }
  },
  computed: {
    group () {
      return _.chunk(this.list, this.count);
    }
  },
  mounted () {
    if (!_.isEmpty(this.list)) this.toIndex = 0;
  },
  methods: {
    setIndex (index) {
      if (!HB.is_mobile) {
        this.toIndex = index;
      }
    },
    prev () {
      if (this.current.page !== 0)
        this.toIndex = this.current.page - 1
    },
    next () {
      if (this.current.page !== this.group.length - 1)
        this.toIndex = this.current.page + 1
    }
  },
  watch: {
    list (value) {
      if (!_.isEmpty(value)) this.toIndex = 0;
    }
  },
  directives: {
    slider: require('../directives/slider')
  }
};