module.exports = {
  template: require('../template/shop.review.pug'),
  data () {
    return {
      collapse: HB.is_mobile
    }
  },
  computed: {
    shop_info () {
      return this.$store.getters.shop_info;
    },
    posts () {
      const posts = this.$store.getters.posts;
      return this.collapse ? posts.slice(0, 5) : posts;
    },
    hasMore () {
      return this.$store.getters.posts.length > 5
    }
  },
  methods: {
    url (url) {
      return `url(${url})`
    },
    expand () {
      this.collapse = false
    }
  }
};