module.exports = {
  props: ['managed', 'prepay'],
  computed: {
    shop_id () {
      return this.$store.getters.shop_info.id;
    },
    link () {
      return `http://heybeauty.me/app/?token=117863359&source=landing_shop_detail&medium=detect_client&campaign=shop_id_${this.shop_id}`;
    },
    resource () {
      if (_.isUndefined(this.shop_id))
        return null;
      else if (this.prepay === true)
        return 'http://assets.heybeauty.me/kr/web/banner/prepay.jpg';
      else if (this.managed === true)
        return 'http://assets.heybeauty.me/kr/web/banner/managed.jpg';
      else
        return 'http://assets.heybeauty.me/kr/web/banner/unmanaged.jpg';
    }
  },
  template: `
    <div id="banner" v-show="shop_id">
      <a :href="link">
        <img :src="resource">
      </a>
    </img>
  `
};