import moment from 'moment'

module.exports = {
  template: require('../template/popup.service.pug'),
  data () {
    return {}
  },
  computed: {
    service () {
      let service = _.find(this.$store.getters.services, {
        id: Number(this.$route.params.service_id)
      }) || {};

      const start = moment(service.event_start_ts, 'X');
      const end = moment(service.event_end_ts, 'X');

      service.is_event = service.is_event && moment().isBetween(start, end, null, '[]');

      return service;
    },
    portfolios () {
      return _.filter(this.$store.getters.portfolios, {
        service_id: Number(this.$route.params.service_id)
      });
    }
  },
  methods: {
    url (url) {
      return `url(${url})`;
    },
    close () {
      if (this.$route.query.from === 'info')
        this.$router.push({ name: 'shop.service' });
      else if (this.$route.query.from === 'portfolio')
        this.$router.go(-2);
      else
        this.$router.back();

    }
  },
  directives: {
    popup: require('../directives/popup')
  },
  components: {
    'hb-slider': require('./slider')
  }
};