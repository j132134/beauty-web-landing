import moment from 'moment'

module.exports = {
  template: require('../template/popup.portfolio.pug'),
  data () {
    return {}
  },
  computed: {
    portfolio () {
      return _.find(this.$store.getters.portfolios, {
        id: Number(this.$route.params.portfolio_id)
      }) || {};
    },
    service () {
      let service = _.find(this.$store.getters.services, {
        id: Number(this.portfolio.service_id)
      });

      if (_.isEmpty(service)) return null;

      const start = moment(service.event_start_ts, 'X');
      const end = moment(service.event_end_ts, 'X');

      service.is_event = service.is_event && moment().isBetween(start, end, null, '[]');

      return service;
    },
    portfolios () {
      return this.$store.getters.portfolios;
    },
    index () {
      return _.findIndex(this.portfolios, {
        id: Number(this.$route.params.portfolio_id)
      });
    }
  },
  methods: {
    url (url) {
      return `url(${url})`
    },
    close () {
      if (this.$route.query.from === 'info')
        this.$router.push({name: 'shop.service'});
      else
        this.$router.back();
    }
  },
  updated () {
    if (this.index == -1)
      this.$router.replace({name: 'shop.service'});
  },
  directives: {
    popup: require('../directives/popup'),
    'init-scroll': {
      componentUpdated (el, binding) {
        const $el = $(el);
        if (HB.is_mobile) {
          const to = $el.scrollLeft() + $el.children().eq(binding.value.index).offset().left - $el.offset().left;
          $el.animate({scrollLeft: to - 45}, 300);
        }
        else {
          const to = $el.scrollTop() + $el.children().eq(binding.value.index).offset().top - $el.offset().top;
          $el.animate({scrollTop: to - 70}, 300);
        }
      }
    }
  }
};