import moment from 'moment'
import popup from '../popup'

function deg2rad(deg) {
  return deg * (Math.PI/180);
}
function getDistance (lat1, lng1, lat2, lng2) {
  const R = 6371;
  const dLat = deg2rad(lat2-lat1);
  const dLon = deg2rad(lng2-lng1);
  const a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  return R * c;
}

module.exports = {
  template: require('../template/shop.info.pug'),
  data () {
    return {}
  },
  computed: {
    shop_info () {
      return this.$store.getters.shop_info;
    },
    services () {
      return _.chain(this.$store.getters.services)
        .each((service) => {
          const start = moment(service.event_start_ts, 'X');
          const end = moment(service.event_end_ts, 'X');
          service.is_event = service.is_event && moment().isBetween(start, end, null, '[]');
          service.portfolio_count = _.filter(this.$store.getters.portfolios, {service_id: service.id}).length;
        })
        .orderBy(['is_event', 'count'], ['desc', 'desc'])
        .slice(0, 4)
        .value();
    },
    portfolios () {
      return this.$store.getters.portfolios.slice(0, 4);
    },
    posts () {
      return this.$store.getters.posts.slice(0, 2);
    },
    nearby () {
      return _.chain(this.$store.getters.nearby)
        .each((shop) => {
          shop.distance = getDistance(this.shop_info.latitude, this.shop_info.longitude, shop.latitude, shop.longitude);
          if (_.isEmpty(shop.image_url)) shop.image_url.push(`http://assets.heybeauty.me/kr/web/cover/cat_${this.shop_info.category_id_list[0]}_${_.random(0, 3)}.jpg`);
        })
        .orderBy('distance', 'asc')
        .value();
    },
    events () {
      return _.each(this.$store.getters.events, (event) => {
        _.each(event.shops, (shop) => {
          if (_.isEmpty(shop.image_url)) shop.image_url.push(`http://assets.heybeauty.me/kr/web/cover/cat_${shop.category_id_list[0]}_${_.random(0, 3)}.jpg`)
        });
      });
    },
    banners () {
      return _.filter(this.$store.getters.banners, (banner) => {
        return Boolean(banner.web_image_url)
      });
    },
    map () {
      if (_.isEmpty(this.shop_info)) return {};
      const api = 'https://openapi.naver.com/v1/map/staticmap.bin';
      const id = 'T8YgEgUdPbDJikeG8HHJ';
      const url = encodeURIComponent('http://lvh.me:8000/');
      const mobile_param = 'level=9&scale=2&w=298&h=96&baselayer=default';
      const desktop_param = 'level=11&scale=2&w=390&h=140&baselayer=default';
      return {
        image_url: `${api}?clientId=${id}&url=${url}&crs=EPSG:4326&center=${this.shop_info.longitude},${this.shop_info.latitude}&${HB.is_mobile ? mobile_param : desktop_param}`,
        link: encodeURI(`http://map.daum.net/link/map/${this.shop_info.name},${this.shop_info.latitude},${this.shop_info.longitude}`)
      }
    }
  },
  methods: {
    url (url) {
      return `url(${url})`
    },
    transition (state, hash = '') {
      this.$router.push({name: `shop.${state}`, params: this.$router.params, hash})
    },
    open (target, index) {
      let url;

      if (target === 'banners' && this.banners[index].link)
        url = this.banners[index].link;
      else
        url = `/content/${_.get(this, `${target}[${index}].id`)}`;

      if (HB.is_mobile)
        location.href = url;
      else
        popup.open(url);
    }
  },
  directives: {
    'event-banner': require('../directives/eventbanner')
  },
  components: {
    'hb-slider': require('./slider')
  }
};