import moment from 'moment'

module.exports = {
  template: require('../template/shop.service.pug'),
  data () {
    return {}
  },
  computed: {
    portfolios () {
      return this.$store.getters.portfolios;
    },
    portfolios_count () {
      return `총 ${this.portfolios.length}개`
    },
    services () {
      return _.chain(this.$store.getters.services)
        .each((service) => {
          const start = moment(service.event_start_ts, 'X');
          const end = moment(service.event_end_ts, 'X');
          service.is_event = service.is_event && moment().isBetween(start, end, null, '[]');
          service.portfolio_count = _.filter(this.$store.getters.portfolios, {service_id: service.id}).length;
          service.service_category_name = this.$store.getters.service_category_name(service.service_category_id) || '미분류';
        })
        .orderBy(['is_event', 'count'], ['desc', 'desc'])
        .groupBy('service_category_name')
        .value();
    },
    empty_service () {
      return !this.$store.getters.services.length
    }
  },
  methods: {
    url (url) {
      return `url(${url})`
    }
  },
  components: {
    'hb-slider': require('./slider')
  }
};