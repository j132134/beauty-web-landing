import meta from '../meta'
import popup from '../popup'

module.exports = {
  template: require('../template/shop.common.pug'),
  data () {
    return {
      error: 0
    }
  },
  head: {
    title () {
      return {
        inner: this.shop_info.name || ''
      }
    },
    meta () {
      return meta.toArray(this.shop_info);
    }
  },
  created () {
    this.getShop(this.$route.params.id);
  },
  computed: {
    shop_info () {
      return this.$store.getters.shop_info;
    },
    event () {
      return _.find(this.$store.getters.events, (event) => {
        return _.includes(_.map(event.shops, 'id'), Number(this.$route.params.id));
      });
    }
  },
  methods: {
    url (url) {
      return `url(${url})`;
    },
    getShop (id) {
      this.$store.dispatch('getShop', id).then(() => {
        this.$emit('updateHead')
      }, () => {
        this.error = 404;
      });
    },
    open () {
      let url = `/content/${this.event.id}`;

      if (HB.is_mobile)
        location.href = url;
      else
        popup.open(url);
    }
  },
  beforeRouteEnter (to, from, next) {
    $.get('https://api.heybeauty.kr/web/app/meta_info').then((response) => {
      next(vm => {
        vm.$store.dispatch('setMeta', response.data);
      })
    }, (response) => {
      console.error('메타 정보를 가져오는데 에러가 발생했습니다.');
      console.debug(response);
      next(vm => {
        vm.error = 500;
      })
    });
  },
  watch: {
    $route (to, from) {
      if (to.params.id !== from.params.id)
        this.getShop(to.params.id)
    }
  },
  directives: {
    global: require('../directives/global'),
    header: require('../directives/header')
  },
  components: {
    'hb-banner': require('./banner')
  }
};