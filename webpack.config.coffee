_ = require 'lodash'
path = require 'path'
webpack = require 'webpack'

config = require './gulp.config'
src = config.path.src

base =
  entry:
    user: path.join src.coffee, 'user/entry.coffee'
    app: path.join src.coffee, 'app/main.js'

  output:
    filename: '[name].js'

  module:
    rules: [
      {
        test: /\.js/
        exclude: /node_modules/
        use:
          loader: 'babel-loader'
          options:
            presets: ['es2015']
      }
      {
        test: /\.coffee$/
        use: 'coffee-loader'
      }
      {
        test: /\.pug$/
        use: 'pug-html-loader'
      }
    ]

  resolve:
    modules: ['node_modules', path.join __dirname, 'src']
    alias:
      'lodash': path.join __dirname, 'node_modules/lodash/lodash.min.js'
      'vue$': path.join __dirname, 'node_modules/vue/dist/vue.common.js'

plugins =
  banner: [
    new webpack.BannerPlugin({
      banner: '"use strict";'
      raw: true
    })
  ]
  shim: [
    new webpack.ProvidePlugin({
      _: 'lodash'
      $: 'jquery'
      Promise: 'es6-promise-promise'
    })
  ]
  debug: [
    new webpack.LoaderOptionsPlugin({
      minimize: false
      debug: true
    })
  ]
  uglify: [
    new webpack.optimize.UglifyJsPlugin({
      beautify: false
      mangle: true
      sourceMap: false
      comments: false
      compress:
        drop_debugger: false
        hoist_funs: true
        hoist_vars: true
        cascade: false
    })
  ]

module.exports =
  dev: _.assign
    cache: false
    devtool: 'source-map'
    plugins: _.concat plugins.banner, plugins.shim, plugins.debug
  , base

  prod: _.assign
    cache: false
    plugins: _.concat plugins.banner, plugins.shim, plugins.uglify
  , base
